variable "s3_bucket" {
	description = "Bucket for lambda function artifacts"
	type        = string
}

variable "s3_key" {
	description = "The S3 key with full path to the ZIP file for the lambda package"
	type        = string
}

variable "function_name" {
	description = "Name for the lambda function"
	type        = string
}

variable "timeout" {
	description = "Max timeout for the lambda function to run"
	type        = string
}

variable "memory_size" {
	description = "Amount of memory in GB to allocate to a single execution"
	type        = string
}

variable "security_group_ids" {
	description = "The security groups to attach to the Lambda"
	type        = list
}

variable "subnet_ids" {
	description = "The security groups to attach to the Lambda"
	type        = list
}

variable "role_name" {
	description = "IAM role to attach to the lambda"
	type        = string
}

variable "handler" {
	description = "Function to run on execution"
	type        = string
}

variable "environment_variables" {
	description = "Environment variables used by the Lambda runtime environment"
	type        = map
}