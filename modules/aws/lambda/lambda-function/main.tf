
resource "aws_lambda_function" "lambda_function" {
  s3_bucket      = "${var.s3_bucket}"
  s3_key         = "${var.s3_key}"
  function_name  = "${var.function_name}"
  role           = "${data.aws_iam_role.role.arn}"
  handler        = "${var.handler}"
  runtime        = "python3.8"
  timeout        = "${var.timeout}"
  memory_size    = "${var.memory_size}"

  vpc_config {
    subnet_ids         = var.subnet_ids
    security_group_ids = var.security_group_ids
  }

  dynamic "environment" {
    for_each = length(var.environment_variables) > 0 ? [true] : []

    content {
      variables = var.environment_variables
    }
  }
}

output "function_name" {
  value = aws_lambda_function.lambda_function.function_name
}

output "arn" {
  value = aws_lambda_function.lambda_function.arn
}