#!/bin/bash

##################################
##### SETUP DIRECTORIES/USERS ####
##################################

mkdir -p /home/hddcoin/supermine/logs
mkdir -p /home/hddcoin/supermine/xch

useradd -m hddcoin
usermod -aG sudo hddcoin
echo 'hddcoin ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

##################################
##### CLOUDWATCH AGENT SETUP #####
##################################

curl https://s3.${aws_region}.amazonaws.com/amazoncloudwatch-agent-${aws_region}/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb --output amazon-cloudwatch-agent.deb
dpkg -i amazon-cloudwatch-agent.deb


echo '{
      "agent": {
        "logfile": "/opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log",
        "debug": true,
        "run_as_user": "cwagent"
      },
      "logs": {
        "logs_collected": {
          "files": {
            "collect_list": [
              {
                "file_path": "/home/hddcoin/supermine/logs/txns.log",
                "log_group_name": "/ec2/Payouts/SuccessfulTxns",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/hddcoin/supermine/logs/sql_errors.log",
                "log_group_name": "/ec2/Payouts/SQLErrors",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/hddcoin/supermine/logs/txns_errors.log",
                "log_group_name": "/ec2/Payouts/FailedTxns,
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/hddcoin/supermine/logs/sync_errors.log",
                "log_group_name": "/ec2/Payouts/SyncErrors",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/hddcoin/supermine/logs/secretsmanager_errors.log",
                "log_group_name": "/ec2/Payouts/SecretsManagerErrors",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              }
            ]
          }
        },
        "log_stream_name": "/ec2/catchall"
      }
}
' >> /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json

/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:///opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json


##################################
######## MOUNT EBS VOLUME ########
##################################

# add while ls /dev/xvdh errors
# sleep
sleep 15

mkdir -p /home/hddcoin
${mkfs_cmd}
mount /dev/nvme1n1 /home/hddcoin
chmod -R 777 /home/hddcoin

##################################
#### INSTALL hddcoin BLOCKCHAIN #####
##################################

apt update -y
apt upgrade -y
apt install python3-pip  -y
apt install git -y

pip3 install --ignore-installed awscli

# overwrite the file
echo 'cd /home/hddcoin
git clone https://github.com/HDDcoin-Network/hddcoin-blockchain.git
cd hddcoin-blockchain
sh install.sh

. ./activate
hddcoin init --fix-ssl-permissions
hddcoin keys delete_all
echo '${hddcoin_key}' > ${hddcoin_key_filename}
hddcoin keys add -f ${hddcoin_key_filename}
rm ${hddcoin_key_filename}
aws s3 cp s3://supermine-artifacts-{env}/.hddcoin/mainnet /home/hddcoin/.hddcoin/ --recursive
aws s3 cp s3://supermine-artifacts-{env}/.hddcoin/wallet /home/hddcoin/.hddcoin/mainnet/ --recursive
hddcoin start node
hddcoin start wallet

hddcoin show -a node-1.hddcoin.org:28444
hddcoin show -a node-2.hddcoin.org:28444
hddcoin show -a pool-node-1.hddcoin.org:28444
hddcoin show -a pool-node-2.hddcoin.org:28444
deactivate
' >> /home/hddcoin/startup.sh

chmod +x /home/hddcoin/startup.sh

sudo -u hddcoin -H sh -c "/home/hddcoin/startup.sh > /home/hddcoin/startup.log"

# remove hddcoin user from sudoers file
#head -n -1 /etc/sudoers > temp.txt ; mv temp.txt /etc/sudoers

##################################
####### RUN PAYOUTS SCRIPT #######
##################################

# pip3 install mysql-connector-python
# pip3 install lambda-cache
# pip3 install boto3

# at the end payouts, stop hddcoin and unmount EBS
# then kill the instance through Lambda
# what happens when we increase the EBS volume size?

# cd ~
# git clone https://gitlab.com/alphak3y/supermine-lambda-kinesis-consumer
# cd supermine-lambda-kinesis-consumer
# python3 payouts.py



