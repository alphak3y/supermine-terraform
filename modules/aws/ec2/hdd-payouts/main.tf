resource "aws_instance" "payouts" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  iam_instance_profile   = var.iam_instance_profile_name
  monitoring = var.monitoring_enabled

  vpc_security_group_ids = var.security_group_ids
  subnet_id              = var.subnet_id

  key_name = var.ec2_host_key_pair

  user_data = base64encode(templatefile("${path.module}/user_data.sh", {
   aws_region              = "us-east-1"
   hddcoin_key                = var.hddcoin_key
   hddcoin_key_filename       = var.hddcoin_key_filename,
   mkfs_cmd                = var.mkfs_cmd
  }))

  tags = {
    Name = "${var.instance_name}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

## network interface attachment

resource "aws_ebs_volume" "sync" {
  availability_zone = "us-east-1c"
  size              = 20

  tags = {
    Name = "HDDCoinNodeSync"
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.sync.id
  instance_id = aws_instance.payouts.id
  force_detach = false
}

output "private_ip" {
  value = aws_instance.payouts.private_ip
}