variable "instance_name" {
  description = "Name for the EC2"
  type        = string
}

variable "ami_id" {
  description = "ID for the EC2 AMI"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "instance type for the EC2"
  type        = string
  default     = "t3.nano"
}

variable "monitoring_enabled" {
  description = "Set to true for Cloudwatch monitoring"
  default     = false
}

variable "ec2_host_key_pair" {
  description = "the KMS key to use for ssh access"
  type        = string
}

variable "security_group_ids" {
  type    = list
  default = []
}

variable "vpc_id" {
  type    = string
}

variable "subnet_id_1" {
  type    = string
}

variable "subnet_id_2" {
  type    = string
}
