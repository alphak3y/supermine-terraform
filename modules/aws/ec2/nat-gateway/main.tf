
data "aws_vpc" "main" {
  id = var.vpc_id
}

data "aws_subnet" "private_1" {
  id = var.subnet_id_1
}

data "aws_subnet" "private_2" {
  id = var.subnet_id_2
}

data "aws_internet_gateway" "default" {
  filter {
    name   = "attachment.vpc-id"
    values = [var.vpc_id]
  }
}

resource "aws_instance" "nat_gateway" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  monitoring             = var.monitoring_enabled

  network_interface  {
    network_interface_id = aws_network_interface.nat_gateway.id
    device_index         = 0
  }    

  key_name               = var.ec2_host_key_pair

  #  user_data = base64encode(templatefile("${path.module}/user_data.sh", {}))

  tags = {
    Name = "${var.instance_name}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_network_interface" "nat_gateway" {
  subnet_id   = aws_subnet.nat_gateway.id
  description = "NAT Gateway"
  source_dest_check = false
}


resource "aws_eip" "nat_gateway" {
  vpc                       = true
  network_interface         = aws_network_interface.nat_gateway.id
}

resource "aws_subnet" "nat_gateway" {
  vpc_id     = data.aws_vpc.main.id
  cidr_block = "172.31.100.0/26"
  map_public_ip_on_launch = false

  tags = {
    Name = "NAT Gateway"
  }
}

resource "aws_route_table_association" "nat_gateway" {
  subnet_id      = aws_subnet.nat_gateway.id
  route_table_id = aws_route_table.nat_gateway.id
}

resource "aws_route_table" "nat_gateway" {
  vpc_id = data.aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.default.id
  }

  tags = {
    Name = "NAT Gateway to Internet Gateway"
  }
}

resource "aws_route_table_association" "private_to_nat_gateway_1" {
  subnet_id      = data.aws_subnet.private_1.id
  route_table_id = aws_route_table.private_to_nat_gateway.id
}

resource "aws_route_table_association" "private_to_nat_gateway_2" {
  subnet_id      = data.aws_subnet.private_2.id
  route_table_id = aws_route_table.private_to_nat_gateway.id
}

resource "aws_route_table" "private_to_nat_gateway" {
  vpc_id = data.aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    network_interface_id = aws_network_interface.nat_gateway.id
  }

  tags = {
    Name = "Private Subnet to NAT Gateway"
  }
}

output "private_ip" {
  value = aws_instance.nat_gateway.private_ip
}