#!/bin/bash

##################################
##### SETUP DIRECTORIES/USERS ####
##################################

mkdir -p /home/chia/supermine/logs
mkdir -p /home/chia/supermine/xch

useradd -m chia
usermod -aG sudo chia
echo 'chia ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

##################################
##### CLOUDWATCH AGENT SETUP #####
##################################

curl https://s3.${aws_region}.amazonaws.com/amazoncloudwatch-agent-${aws_region}/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb --output amazon-cloudwatch-agent.deb
dpkg -i amazon-cloudwatch-agent.deb


echo '{
      "agent": {
        "logfile": "/opt/aws/amazon-cloudwatch-agent/logs/amazon-cloudwatch-agent.log",
        "debug": true,
        "run_as_user": "cwagent"
      },
      "logs": {
        "logs_collected": {
          "files": {
            "collect_list": [
              {
                "file_path": "/home/chia/supermine/logs/txns.log",
                "log_group_name": "/ec2/Payouts/SuccessfulTxns",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/chia/supermine/logs/sql_errors.log",
                "log_group_name": "/ec2/Payouts/SQLErrors",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/chia/supermine/logs/txns_errors.log",
                "log_group_name": "/ec2/Payouts/FailedTxns,
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/chia/supermine/logs/sync_errors.log",
                "log_group_name": "/ec2/Payouts/SyncErrors",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              },
              {
                "file_path": "/home/chia/supermine/logs/secretsmanager_errors.log",
                "log_group_name": "/ec2/Payouts/SecretsManagerErrors",
                "log_stream_name": "{instance_id}_{hostname}",
                "timezone": "UTC"
              }
            ]
          }
        },
        "log_stream_name": "/ec2/catchall"
      }
}
' >> /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json

/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:///opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json


##################################
######## MOUNT EBS VOLUME ########
##################################

# add while ls /dev/xvdh errors
# sleep
sleep 15

mkdir -p /home/chia
${mkfs_cmd}
mount /dev/nvme1n1 /home/chia
chmod -R 777 /home/chia

##################################
#### INSTALL CHIA BLOCKCHAIN #####
##################################

apt update -y
apt upgrade -y
apt install python3-pip  -y
apt install git -y

pip3 install --ignore-installed awscli

echo 'cd /home/chia
git clone https://github.com/Chia-Network/chia-blockchain.git -b latest --recurse-submodules
export DEBIAN_FRONTEND=noninteractive
cd chia-blockchain
sh install.sh

. ./activate

pip3 install mysql-connector
pip3 install lambda-cache
pip3 install boto3
pip3 install requests


chia init
chia keys delete_all
echo '${chia_key}' > ${chia_key_filename}
chia keys add -f ${chia_key_filename}
rm ${chia_key_filename}
aws s3 cp s3://supermine-artifacts-{env}/db/ /home/chia/.chia/ --recursive
chia start node
chia start wallet
curl https://chia.keva.app/ | grep -Eo \'[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}' | while read line; do timeout 5s chia show -a $line:8444 ;done
deactivate
' >> /home/chia/startup.sh

chmod +x /home/chia/startup.sh

sudo -u chia -H sh -c "/home/chia/startup.sh > /home/chia/startup.log"

# remove chia user from sudoers file
#head -n -1 /etc/sudoers > temp.txt ; mv temp.txt /etc/sudoers

##################################
####### RUN PAYOUTS SCRIPT #######
##################################



# at the end payouts, stop chia and unmount EBS
# then kill the instance through Lambda
# what happens when we increase the EBS volume size?




