variable "instance_name" {
  description = "Name for the EC2"
  type        = string
}

variable "ami_id" {
  description = "ID for the EC2 AMI"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "instance type for the EC2"
  type        = string
  default     = "t3.nano"
}

variable "iam_instance_profile_name" {
  description = "IAM instance profile for the EC2"
  type        = string
}

variable "monitoring_enabled" {
  description = "Set to true for Cloudwatch monitoring"
  default     = false
}

variable "ec2_host_key_pair" {
  description = "the KMS key to use for ssh access"
  type        = string
}

variable "ebs_volume_size" {
  description = "The size in GB of the EBS voluem"
  type        = string
  default     = "40"
}

variable "security_group_ids" {
  type    = list
  default = []
}

variable "subnet_id" {
  type    = string
}

variable "chia_key_filename" {
  type   = string
  sensitive = true
}

variable "chia_key" {
  type   = string
  sensitive = true
}

variable "mkfs_cmd" {
  type   = string
  default = ""
}

variable "env" {
  type = string
}