variable "stream_name" {
  type        = string
}

variable "db_identifier" {
  type        = string
}

variable "lambda_role" {
  type        = string
}

variable "lambda_function_name" {
  type        = string
}

variable "batch_size" {
  type        = string
}

variable "starting_position" {
  type        = string
}

variable "enabled" {
  type        = string
}