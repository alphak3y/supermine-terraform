
#kinesis event mapping
resource "aws_lambda_event_source_mapping" "lambda_event_mapping" {
  event_source_arn  = "${data.aws_kinesis_stream.farmer_api_stream.arn}"
  function_name     = var.lambda_function_name
  batch_size        = var.batch_size
  starting_position = var.starting_position
  enabled           = var.enabled
}
