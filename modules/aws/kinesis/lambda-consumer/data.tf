data "aws_kinesis_stream" "farmer_api_stream" {
  name = var.stream_name
}

data "aws_db_instance" "db_instance"{
  db_instance_identifier = var.db_identifier
}

data "aws_iam_role" "lambda_role"{
  name = var.lambda_role
}

data "aws_secretsmanager_secret" "farmer_db_password" {
  name = "rds_password"
}

data "aws_secretsmanager_secret" "farmer_db_username" {
  name = "rds_username"
}

data "aws_security_group" "default" {
  name = "default"
}

data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}
