variable "api_resource_path" {
	description = "The path for the API resource created"
	type        = string
}

variable "aws_api_gateway_rest_api_name" {
	description = "REST API name from API Gateway"
	type        = string
}

variable "account_number" {
	type        = string
	default     = "978911255009"
}

variable "lambda_s3_bucket" {
	description = "Bucket for lambda function artifacts"
	type        = string
}

variable "lambda_s3_key" {
	description = "The S3 key with full path to the ZIP file for the lambda package"
	type        = string
}

variable "lambda_function_name" {
	description = "Name for the lambda function"
	type        = string
}

variable "lambda_handler_function" {
	description = "The function which will run when the lambda is executed"
	type        = string
	default     = "lambda_function.lambda_handler"
}

variable "lambda_timeout" {
	description = "Max timeout for the lambda function to run"
	type        = string
}

variable "lambda_memory_size" {
	description = "Amount of memory in GB to allocate to a single execution"
	type        = string
}

variable "security_group_ids" {
	description = "The security groups to attach to the Lambda"
	type        = list
	default     = ["sg-bd04eda4"]
}

variable "subnet_ids" {
	description = "The security groups to attach to the Lambda"
	type        = list
	default     = ["subnet-27eff96a","subnet-0acf5408a315f36bd"]
}

variable "lambda_role_name" {
	description = "IAM role to attach to the lambda"
	type        = string
}

variable "environment_variables" {
	description = "Environment variables used by the Lambda runtime environment"
	type        = map
}

variable "alias_name" {
	description = ""
	type        = string
}

variable "alias_version" {
	description = ""
	type        = string
}
