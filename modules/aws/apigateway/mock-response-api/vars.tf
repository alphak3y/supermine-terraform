variable "api_resource_path" {
	description = "The path for the API resource created"
	type        = string
}

variable "aws_api_gateway_rest_api_name" {
	description = "REST API name from API Gateway"
	type        = string
}