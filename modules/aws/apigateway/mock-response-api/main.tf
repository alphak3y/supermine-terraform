data "aws_api_gateway_rest_api" "rest_api" {
  name = "${var.aws_api_gateway_rest_api_name}"
}

resource "aws_api_gateway_resource" "rest_api_resource" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  parent_id   = data.aws_api_gateway_rest_api.rest_api.root_resource_id
  path_part   = "${var.api_resource_path}"
}

resource "aws_api_gateway_method" "rest_api_method" {
  rest_api_id   = data.aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.rest_api_resource.id
  http_method   = "GET"
  authorization = "NONE"

  depends_on = [
    aws_api_gateway_resource.rest_api_resource
  ]
}

resource "aws_api_gateway_integration" "mock" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.rest_api_method.http_method}"
  type = "MOCK"

  request_templates = {
    "application/json": "{\"statusCode\": 200}" 
    /*"*/
  }

  depends_on = [
    aws_api_gateway_method.rest_api_method
  ]
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_api_resource.id
  http_method = "GET"
  status_code = 200
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }

  depends_on = [
    aws_api_gateway_method.rest_api_method
  ]
}

resource "aws_api_gateway_integration_response" "rest_api_response" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_api_resource.id
  http_method = aws_api_gateway_method.rest_api_method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }

  response_templates = {
        "application/json" = jsonencode({
          status: "ok",
          poolInfo: {
            fee: 0.75,
            feeType: "PPLNS",
            minPay: 0.05,
            title: "Supermine",
            "url": "https://supermine.io",
            "nodes": [
              "supermine.io"
            ]
          }
          miners: 0,
          capacityBytes: 1,
          capacityString: "0 TB",
          minedBlocks: [
            {miner:"",number:"",height:"467694",time:"2021-06-22 11:56:35"}
          ]
        })
  }

  depends_on = [
    aws_api_gateway_method.rest_api_method,
    aws_api_gateway_integration.mock
  ]
}



resource "aws_api_gateway_method" "opt" {
  rest_api_id   = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id   = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method   = "OPTIONS"
  authorization = "NONE"

  request_parameters = {
    "method.request.header.Access-Control-Allow-Origin" = true,
    "method.request.header.Access-Control-Allow-Methods" = true,
    "method.request.header.Access-Control-Allow-Headers" = true
  } 
}

resource "aws_api_gateway_integration" "opt" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.opt.http_method}"
  type = "MOCK"

  request_templates = {
    "application/json": "{\"statusCode\": 200}"
  }

  depends_on = [
    aws_api_gateway_method.opt
  ]
}

resource "aws_api_gateway_integration_response" "opt" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.opt.http_method}"
  status_code = 200
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'",
    "method.response.header.Access-Control-Allow-Headers" = "'Access-Control-Allow-Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Requested-With'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS,POST'"
  }
  depends_on = [aws_api_gateway_integration.opt, aws_api_gateway_method_response.opt]
}

resource "aws_api_gateway_method_response" "opt" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.opt.http_method}"
  status_code = 200
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Headers" = true
  }
  response_models = {
    "application/json" = "Empty"
  }
  depends_on = [aws_api_gateway_method.opt]
}

output "http_method" {
  value = aws_api_gateway_method.rest_api_method.http_method
}

output "resource_id" {
  value = aws_api_gateway_resource.rest_api_resource.id
}