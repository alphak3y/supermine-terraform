data "aws_api_gateway_rest_api" "rest_api" {
  name = "${var.aws_api_gateway_rest_api_name}"
}

data "aws_iam_role" "rest_api_lambda" {
  name = "${var.lambda_role_name}"
}

data "aws_security_group" "default" {
  name = "default"
}

data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}

#lambda function
resource "aws_lambda_function" "rest_api" {
  s3_bucket      = "${var.lambda_s3_bucket}"
  s3_key         = "${var.lambda_s3_key}"
  function_name  = "${var.lambda_function_name}"
  role           = "${data.aws_iam_role.rest_api_lambda.arn}"
  handler        = "lambda_function.lambda_handler"
  runtime        = "python3.8"
  timeout        = "${var.lambda_timeout}"
  memory_size    = "${var.lambda_memory_size}"

  vpc_config {
    subnet_ids         = [data.aws_subnet.private_1b.id,data.aws_subnet.private_1c.id]
    security_group_ids = [data.aws_security_group.default.id]
  }

  dynamic "environment" {
    for_each = length(var.environment_variables) > 0 ? [true] : []

    content {
      variables = var.environment_variables
    }
  }
}

resource "aws_api_gateway_resource" "rest_api_resource" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  parent_id   = data.aws_api_gateway_rest_api.rest_api.root_resource_id
  path_part   = "${var.api_resource_path}"
}

resource "aws_api_gateway_method" "rest_api_method" {
  rest_api_id   = data.aws_api_gateway_rest_api.rest_api.id
  resource_id   = aws_api_gateway_resource.rest_api_resource.id
  http_method   = var.http_method
  authorization = "NONE"

  depends_on = [
    aws_api_gateway_resource.rest_api_resource
  ]
}

resource "aws_api_gateway_method" "opt" {
  rest_api_id   = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id   = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method   = "OPTIONS"
  authorization = "NONE"

  request_parameters = {
    "method.request.header.Access-Control-Allow-Origin" = true,
    "method.request.header.Access-Control-Allow-Methods" = true,
    "method.request.header.Access-Control-Allow-Headers" = true
  } 
}

resource "aws_api_gateway_integration" "opt" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.opt.http_method}"
  type = "MOCK"

  request_templates = {
    "application/json": "{\"statusCode\": 200}"
  }

  depends_on = [
    aws_api_gateway_method.opt
  ]
}

resource "aws_api_gateway_integration_response" "opt" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.opt.http_method}"
  status_code = 200
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'",
    "method.response.header.Access-Control-Allow-Headers" = "'Access-Control-Allow-Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Requested-With'",
    "method.response.header.Access-Control-Allow-Methods" = "'GET,OPTIONS,POST'"
  }
  depends_on = [aws_api_gateway_integration.opt, aws_api_gateway_method_response.opt]
}

resource "aws_api_gateway_method_response" "opt" {
  rest_api_id = "${data.aws_api_gateway_rest_api.rest_api.id}"
  resource_id = "${aws_api_gateway_resource.rest_api_resource.id}"
  http_method = "${aws_api_gateway_method.opt.http_method}"
  status_code = 200
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Headers" = true
  }
  response_models = {
    "application/json" = "Empty"
  }
  depends_on = [aws_api_gateway_method.opt]
}

resource "aws_api_gateway_integration" "rest_api_apigw_lambda_int" {
  rest_api_id             = data.aws_api_gateway_rest_api.rest_api.id
  resource_id             = aws_api_gateway_resource.rest_api_resource.id
  http_method             = aws_api_gateway_method.rest_api_method.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.rest_api.invoke_arn

  depends_on = [
    aws_api_gateway_resource.rest_api_resource
  ]
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_api_resource.id
  http_method = var.http_method
  status_code = 200
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }

  depends_on = [
    aws_api_gateway_integration.rest_api_apigw_lambda_int,
    aws_api_gateway_method.rest_api_method
  ]
}

resource "aws_api_gateway_integration_response" "rest_api_response" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  resource_id = aws_api_gateway_resource.rest_api_resource.id
  http_method = aws_api_gateway_method.rest_api_method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }

  depends_on = [
    aws_api_gateway_integration.rest_api_apigw_lambda_int,
    aws_api_gateway_method.rest_api_method
  ]
}

# Stage - the environment to which the API is deployed

# Route - connects incoming API requests to backend resources.

# Deployment - deploys an API to a stage for clients to access

## TODO: request validator

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.rest_api.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "${data.aws_api_gateway_rest_api.rest_api.execution_arn}/*/${var.http_method}/${var.api_resource_path}"
  # /*/
}

output "http_method" {
  value = aws_api_gateway_method.rest_api_method.http_method
}

output "resource_id" {
  value = aws_api_gateway_resource.rest_api_resource.id
}