data "aws_api_gateway_rest_api" "rest_api" {
  name = "${var.aws_api_gateway_rest_api_name}"
}

data "aws_iam_role" "rest_api_lambda" {
  name = "${var.lambda_role_name}"
}

data "aws_security_group" "default" {
  name = "default"
}


data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}