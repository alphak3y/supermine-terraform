
variable "rest_api_name" {
  description = "Name for the API Gateway REST API"
  type        = string
}

variable "http_method" {
  description = "HTTP method"
  type        = string
}

variable "status_code" {
  description = "Status code for the response"
  type        = string
}

variable "resource_id" {
  description = "Resource id for the integration"
  type        = string
}
