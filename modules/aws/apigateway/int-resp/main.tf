data "aws_api_gateway_rest_api" "rest_api" {
  name = var.rest_api_name
}

resource "aws_api_gateway_method_response" "response" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  resource_id = var.resource_id
  http_method = var.http_method
  status_code = var.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
}

resource "aws_api_gateway_integration_response" "integration_response" {
  rest_api_id = data.aws_api_gateway_rest_api.rest_api.id
  resource_id = var.resource_id
  http_method = var.http_method
  status_code = var.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }

  selection_pattern = ".*${var.status_code}.*"

  depends_on = [
    aws_api_gateway_method_response.response 
  ]
}