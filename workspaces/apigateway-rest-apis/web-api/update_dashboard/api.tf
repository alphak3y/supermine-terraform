data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

module "update-dashboard-api" {
  source = "../../../../modules/aws/apigateway/lambda-int-options/"

  aws_api_gateway_rest_api_name = "web-api"
  api_resource_path             = "update-dashboard"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = "update_dashboard/update_dashboard_1627945426.zip"
  lambda_function_name          = "web-update-dashboard"
  lambda_timeout                = "5"
  lambda_memory_size            = "128"
  lambda_role_name              = "WebAPI"

  environment_variables         = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

module "integration_response_400" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "web-api"
  resource_id   = module.update-dashboard-api.resource_id
  http_method   = module.update-dashboard-api.http_method
  status_code   = "400"

  depends_on = [
    module.update-dashboard-api
  ]
}

module "integration_response_404" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "web-api"
  resource_id   = module.update-dashboard-api.resource_id
  http_method   = module.update-dashboard-api.http_method
  status_code   = "404"

  depends_on = [
    module.update-dashboard-api
  ]
}