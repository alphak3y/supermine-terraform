terraform {
  backend "s3" {
    key            = "web-api/change-token-name/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}