data "aws_iam_role" "web_api" {
  name = "WebAPI"
}

resource "aws_iam_policy_attachment" "lambda_execute_attach" {
  name       = "web_api_execute_lambda"
  roles      = [data.aws_iam_role.web_api.name]
  policy_arn = aws_iam_policy.execute_lambda_in_vpc.arn
}

resource "aws_iam_policy_attachment" "get_secret_attach" {
  name       = "web_api_get_secret"
  roles      = [data.aws_iam_role.web_api.name]
  policy_arn = aws_iam_policy.get_secret_value.arn
}


resource "aws_iam_policy" "get_secret_value" {
  name        = "GetSecretValueRDS"
  path        = "/"
  description = "Allows execution of a Lambda in a VPC"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "secretsmanager:GetRandomPassword",
          "secretsmanager:GetResourcePolicy",
          "secretsmanager:GetSecretValue",
          "secretsmanager:DescribeSecret",
          "secretsmanager:ListSecretVersionIds"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "execute_lambda_in_vpc" {
  name        = "ExecuteLambdaInVpC"
  path        = "/"
  description = "Allows execution of a Lambda in a VPC"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:DescribeNetworkInterfaces",
          "ec2:CreateNetworkInterface",
          "ec2:DeleteNetworkInterface",
          "ec2:DescribeInstances",
          "ec2:AttachNetworkInterface"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}