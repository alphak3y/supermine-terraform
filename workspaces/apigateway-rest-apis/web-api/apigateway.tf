
resource "aws_api_gateway_rest_api" "web_api" {
  name = "web-api"

  description = "beta deployment"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
