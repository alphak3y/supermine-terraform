terraform {
  backend "s3" {

    key            = "web-api/create-farmer-token/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}