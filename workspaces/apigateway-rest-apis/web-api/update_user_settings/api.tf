data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

module "update-user-settings-api" {
  source = "../../../../modules/aws/apigateway/lambda-int-options/"

  aws_api_gateway_rest_api_name = "web-api"
  api_resource_path             = "update-user-settings"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = "update_user_settings/update_user_settings_1627925840.zip"
  lambda_function_name          = "web-update-user-settings"
  lambda_timeout                = "5"
  lambda_memory_size            = "128"
  lambda_role_name              = "WebAPI"

  environment_variables         = {

    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

module "integration_response_400" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "web-api"
  resource_id   = module.update-user-settings-api.resource_id
  http_method   = module.update-user-settings-api.http_method
  status_code   = "400"

  depends_on = [
    module.update-user-settings-api
  ]
}

module "integration_response_304" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "web-api"
  resource_id   = module.update-user-settings-api.resource_id
  http_method   = module.update-user-settings-api.http_method
  status_code   = "304"

  depends_on = [
    module.update-user-settings-api
  ]
}