variable "env" {
	type = string
}

variable "lambda_s3_bucket" {
	type = string
}

variable "db_identifier" {
	type = string
}