data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

module "pool-info-api" {
  source = "../../../../modules/aws/apigateway/lambda-int-options/"

  aws_api_gateway_rest_api_name = "web-api"
  api_resource_path             = "pool-stats"
  http_method                   = "GET"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = var.lambda_s3_key
  lambda_function_name          = "web-pool-info"
  lambda_timeout                = "10"
  lambda_memory_size            = "128"
  lambda_role_name              = "WebAPI"

  environment_variables         = {

    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}