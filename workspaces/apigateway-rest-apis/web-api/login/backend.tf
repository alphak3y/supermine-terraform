terraform {
  backend "s3" {

    key            = "web-api/login/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}