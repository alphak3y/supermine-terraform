terraform {
  backend "s3" {
    key            = "apigateway-rest-apis/deployments/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}