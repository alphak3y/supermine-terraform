
data "aws_api_gateway_rest_api" "farmer_rest_api" {
  name = "xch-farmer-api"
}

data "aws_api_gateway_rest_api" "web_rest_api" {
  name = "web-api"
}

data "aws_lambda_alias" "_0_1_8_init"{
  function_name = "xch-init-farmer-0-1-8"
  name          = "xch-init-farmer-0-1-8"
}

data "aws_lambda_alias" "_0_1_8_memos"{
  function_name = "xch-memos-0-1-8"
  name          = "xch-memos-0-1-8"
}

####################### 0.1.8

resource "aws_api_gateway_deployment" "farmer_rest_api_deployment_0_1_8" {
  rest_api_id = data.aws_api_gateway_rest_api.farmer_rest_api.id

  lifecycle {
    create_before_destroy = true
  }

  triggers = {
    redeployment = sha1(jsonencode(data.aws_api_gateway_rest_api.farmer_rest_api.description))
  }
}

resource "aws_api_gateway_stage" "farmer-rest-api-main-stage_0_1_8" {
  deployment_id = aws_api_gateway_deployment.farmer_rest_api_deployment_0_1_8.id
  rest_api_id   = data.aws_api_gateway_rest_api.farmer_rest_api.id
  stage_name    = "v018"

  variables         = {
    streamName            = "xch-farmer-api-stream-0-1-8"
    initAlias             = "${data.aws_lambda_alias._0_1_8_init.name}"
    memosAlias            = "${data.aws_lambda_alias._0_1_8_memos.name}"
  }
}

################################ Web API Alpha

resource "aws_api_gateway_deployment" "web_rest_api_deployment" {
  rest_api_id = data.aws_api_gateway_rest_api.web_rest_api.id

  lifecycle {
    create_before_destroy = true
  }

  triggers = {
    redeployment = sha1(jsonencode(data.aws_api_gateway_rest_api.farmer_rest_api.description))
  }

}

resource "aws_api_gateway_stage" "web-rest-api-main-stage" {
  deployment_id = aws_api_gateway_deployment.web_rest_api_deployment.id
  rest_api_id   = data.aws_api_gateway_rest_api.web_rest_api.id
  stage_name    = "alpha"
}

################################ Web API Beta

resource "aws_api_gateway_deployment" "web_rest_beta_api_deployment" {
  rest_api_id = data.aws_api_gateway_rest_api.web_rest_api.id

  lifecycle {
    create_before_destroy = true
  }

  triggers = {
    redeployment = sha1(jsonencode(data.aws_api_gateway_rest_api.web_rest_api.description))
  }

}

resource "aws_api_gateway_stage" "web-rest-beta-api-main-stage" {
  deployment_id = aws_api_gateway_deployment.web_rest_beta_api_deployment.id
  rest_api_id   = data.aws_api_gateway_rest_api.web_rest_api.id
  stage_name    = "beta"
}

################################ Pool Stats


resource "aws_api_gateway_deployment" "pool_info_api_deployment" {
  rest_api_id = data.aws_api_gateway_rest_api.web_rest_api.id

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "pool-info-api-main-stage" {
  deployment_id = aws_api_gateway_deployment.pool_info_api_deployment.id
  rest_api_id   = data.aws_api_gateway_rest_api.web_rest_api.id
  stage_name    = "supermine"
}