data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

module "memos-api" {
  source = "../../../../modules/aws/apigateway/lambda-int-alias/"

  aws_api_gateway_rest_api_name = "xch-farmer-api"
  api_resource_path             = "memos"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = ""
  lambda_function_name          = "xch-memos-0-1-8"
  lambda_timeout                = "30"
  lambda_memory_size            = "1024"
  lambda_role_name              = "WebAPI"

  alias_name                    = "xch-memos-0-1-8"
  alias_version                 = "$LATEST"

  environment_variables         = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

module "integration_response_404" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.memos-api.resource_id
  http_method   = module.memos-api.http_method
  status_code   = "404"

  depends_on     = [
    module.memos-api
  ]
}

module "integration_response_403" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.memos-api.resource_id
  http_method   = module.memos-api.http_method
  status_code   = "403"

  depends_on     = [
    module.memos-api
  ]
}

module "integration_response_400" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.memos-api.resource_id
  http_method   = module.memos-api.http_method
  status_code   = "400"

  depends_on     = [
    module.memos-api
  ]
}

module "integration_response_500" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.memos-api.resource_id
  http_method   = module.memos-api.http_method
  status_code   = "500"

  depends_on     = [
    module.memos-api
  ]
}