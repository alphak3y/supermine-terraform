terraform {
  backend "s3" {
    key            = "xch-farmer-api/check_blacklist/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}