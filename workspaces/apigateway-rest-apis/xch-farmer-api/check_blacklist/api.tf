data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

module "check-blacklist-api" {
  source = "../../../../modules/aws/apigateway/lambda-integration/"

  aws_api_gateway_rest_api_name = "xch-farmer-api"
  api_resource_path             = "check-blacklist"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = "check_blacklist/check_blacklist_1625965876.zip"
  lambda_function_name          = "xch-check-blacklist"
  lambda_timeout                = "10"
  lambda_memory_size            = "128"
  lambda_role_name              = "WebAPI"

  environment_variables         = {

    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

module "integration_response_302" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.check-blacklist-api.resource_id
  http_method   = module.check-blacklist-api.http_method
  status_code   = "302"
}

module "integration_response_500" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.check-blacklist-api.resource_id
  http_method   = module.check-blacklist-api.http_method
  status_code   = "500"
}