
data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

data "aws_api_gateway_rest_api" "farmer_api"{
  name = "xch-farmer-api"
}

module "blacklist-user-api" {
  source = "../../../../modules/aws/apigateway/lambda-integration/"

  aws_api_gateway_rest_api_name = "xch-farmer-api"
  api_resource_path             = "xch-blacklist-user"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = "blacklist_user/blacklist_user_1625965705.zip"
  lambda_function_name          = "xch-blacklist-user"
  lambda_timeout                = "10"
  lambda_memory_size            = "128"
  lambda_role_name              = "WebAPI"

  environment_variables         = {

    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

module "integration_response_400" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.blacklist-user-api.resource_id
  http_method   = module.blacklist-user-api.http_method
  status_code   = "400"
}

module "integration_response_409" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.blacklist-user-api.resource_id
  http_method   = module.blacklist-user-api.http_method
  status_code   = "409"
}