terraform {
  backend "s3" {

    key            = "dev/farmer-api/blacklist-user/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}