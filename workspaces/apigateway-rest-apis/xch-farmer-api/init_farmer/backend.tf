terraform {
  backend "s3" {

    key            = "dev/farmer-api/init-farmer/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}