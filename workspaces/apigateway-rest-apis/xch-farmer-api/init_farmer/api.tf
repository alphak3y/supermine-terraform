data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

module "init-farmer-api" {
  source = "../../../../modules/aws/apigateway/lambda-int-alias/"

  aws_api_gateway_rest_api_name = "xch-farmer-api"
  api_resource_path             = "init"
  
  lambda_s3_bucket              = var.lambda_s3_bucket
  lambda_s3_key                 = "init_farmer/init_farmer_1627504729.zip"
  lambda_function_name          = "xch-init-farmer-0-1-8"
  lambda_timeout                = "30"
  lambda_memory_size            = "1024"
  lambda_role_name              = "WebAPI"

  alias_name                    = "xch-init-farmer-0-1-8"
  alias_version                 = "$LATEST"

  environment_variables         = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

module "integration_response_500" {
  source = "../../../../modules/aws/apigateway/int-resp/"

  rest_api_name = "xch-farmer-api"
  resource_id   = module.init-farmer-api.resource_id
  http_method   = module.init-farmer-api.http_method
  status_code   = "500"
} 