provider "aws" {
  region = "us-east-1"
  profile = var.env
}

data "aws_iam_policy" "apigw_cloudwatch" {
  name = "AmazonAPIGatewayPushToCloudWatchLogs"
}

resource "aws_api_gateway_rest_api" "farmer_rest_api" {
  name = "xch-farmer-api"

  description = "0.1.8 deployment"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "farmer_rest_api_resource_update" {
  rest_api_id = aws_api_gateway_rest_api.farmer_rest_api.id
  parent_id   = aws_api_gateway_rest_api.farmer_rest_api.root_resource_id
  path_part   = "update"
}

resource "aws_api_gateway_method" "farmer_rest_api_method" {
  rest_api_id   = aws_api_gateway_rest_api.farmer_rest_api.id
  resource_id   = aws_api_gateway_resource.farmer_rest_api_resource_update.id
  http_method   = "POST"
  authorization = "NONE"

  depends_on = [
    aws_api_gateway_resource.farmer_rest_api_resource_update
  ]
}

resource "aws_api_gateway_integration" "farmer_apigw_kinesis_int" {
  rest_api_id             = aws_api_gateway_rest_api.farmer_rest_api.id
  resource_id             = aws_api_gateway_resource.farmer_rest_api_resource_update.id
  http_method             = aws_api_gateway_method.farmer_rest_api_method.http_method
  credentials             = data.aws_iam_role.farmer_apigw_kinesis_int.arn
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:us-east-1:kinesis:action/PutRecord"

  request_templates = {
    "application/json" = <<EOF
    #set($inputRoot = $input.path('$'))
{
    "Data": $input.json('$.xch_client_info'),
    "PartitionKey": "client-v1",
    "StreamName": "$stageVariables.streamName"
} 
EOF
  }


  depends_on = [
    aws_api_gateway_resource.farmer_rest_api_resource_update
  ]
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.farmer_rest_api.id
  resource_id = aws_api_gateway_resource.farmer_rest_api_resource_update.id
  http_method = "POST"
  status_code = "200"

  depends_on = [
    aws_api_gateway_integration.farmer_apigw_kinesis_int
  ]
}

resource "aws_api_gateway_integration_response" "farmer_rest_api_response" {
  rest_api_id = aws_api_gateway_rest_api.farmer_rest_api.id
  resource_id = aws_api_gateway_resource.farmer_rest_api_resource_update.id
  http_method = aws_api_gateway_method.farmer_rest_api_method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code

  depends_on = [
    aws_api_gateway_integration.farmer_apigw_kinesis_int,
    aws_api_gateway_method.farmer_rest_api_method
  ]
}

module "integration_response_500" {
  source = "../../../modules/aws/apigateway/int-resp/"

  rest_api_name = aws_api_gateway_rest_api.farmer_rest_api.name
  resource_id   = aws_api_gateway_resource.farmer_rest_api_resource_update.id
  http_method   = aws_api_gateway_method.farmer_rest_api_method.http_method
  status_code   = "500"

  depends_on = [
    aws_api_gateway_rest_api.farmer_rest_api
  ]
}

# Stage - the environment to which the API is deployed

# Route - connects incoming API requests to backend resources.

# Deployment - deploys an API to a stage for clients to access

## TODO: request validator


resource "aws_kinesis_stream" "farmer_api_stream_0_1_7" {
  name             = "xch-farmer-api-stream-0-1-7"
  shard_count      = 1
  retention_period = 24

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    Environment = "development"
  }
}

resource "aws_kinesis_stream" "farmer_api_stream_0_1_8" {
  name             = "xch-farmer-api-stream-0-1-8"
  shard_count      = 1
  retention_period = 24

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    Environment = "development"
  }
}