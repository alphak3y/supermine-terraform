
## TODO:
######### security groups
######### parameter group
######### add iam policy to lambda role
######### add security group to lambda
######### db init script CREATE USER 'xch-update-farmer, website-dashboard, website-register, website-login'@...
######### GRANT SELECT,UPDATE,DELETE,INSERT only as needed

resource "aws_db_subnet_group" "xch_farmer" {
  name       = "xch_db"
  subnet_ids = [data.aws_subnet.private_1b.id, data.aws_subnet.private_1c.id]

  tags = {
    Name = "xch_farmer_subnet_group"
  }
}

resource "aws_db_instance" "xch_database" {
  identifier                          = var.db_identifier
  allocated_storage                   = var.db_allocated_storage
  max_allocated_storage               = var.db_max_storage
  engine                              = "mysql"
  engine_version                      = "8.0.20"
  instance_class                      = var.db_instance_class
  name                                = var.db_database_name  
  username                            = "${var.master_username}"
  password                            = "${var.master_password}"
  skip_final_snapshot                 = true
  deletion_protection                 = true
  delete_automated_backups            = true
  iam_database_authentication_enabled = false
  port                                = var.db_port
  publicly_accessible                 = false
  vpc_security_group_ids              = ["${data.aws_security_group.default_sg.id}"]
  multi_az                            = false
  db_subnet_group_name                = "main"
}

resource "aws_db_option_group" "farmer-database-option-group" {
  name                     = "xch-farmer-database-option-group"
  option_group_description = "XCH Farmer Database Option Group"
  engine_name              = "mysql"
  major_engine_version     = "8.0"
}
