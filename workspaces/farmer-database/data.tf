data "aws_iam_role" "xch-update-farmer" {
  name = "XCHUpdateFarmer"
}

data "aws_security_group" "bastion_sg" {
  name = "bastion_to_rds"
}

data "aws_security_group" "default_sg" {
  name = "default"
}

data "aws_vpc" "main" {
  id = var.vpc_id
}

data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}

data "aws_subnet" "bastion" {
  filter {
    name   = "tag:Name"
    values = ["Admin 1c"]
  }
}