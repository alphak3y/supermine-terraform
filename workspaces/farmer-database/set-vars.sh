#! /bin/bash
echo 'Enter RDS master username: '
read -s master_username
export TF_VAR_master_username=$master_username

echo 'Enter RDS master password: '
read -s master_password
export TF_VAR_master_password=$master_password