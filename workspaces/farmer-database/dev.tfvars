
env     = "dev"
vpc_id  = "vpc-73e3710e"

db_identifier        = "farmer-database"
db_database_name     = "xch_farmer_database"
db_allocated_storage = "20"
db_max_storage       = "30"
db_instance_class    = "db.t2.small"
db_port              = "5400"


