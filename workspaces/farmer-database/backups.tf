
resource "aws_backup_vault" "xch_db_backups" {
  name        = "xch_rds_backup_vault"
}

resource "aws_backup_plan" "xch_db_backups" {
  name = "xch_rds_backup_plan"

  rule {
    rule_name                = "tf_xch_db_backup"
    target_vault_name        = aws_backup_vault.xch_db_backups.name
    schedule                 = "cron(0 * * * ? *)"
    enable_continuous_backup = true
    lifecycle {
      delete_after = 7
    }
  }


}

resource "aws_iam_role" "xch_db_backups" {
  name               = "XCHDBBackups"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": ["sts:AssumeRole"],
      "Effect": "allow",
      "Principal": {
        "Service": ["backup.amazonaws.com"]
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "example" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.xch_db_backups.name
}

resource "aws_backup_selection" "example" {
  name         = "xch_db_backup_selection"
  plan_id      = aws_backup_plan.xch_db_backups.id
  iam_role_arn = aws_iam_role.xch_db_backups.arn

  resources = [
    aws_db_instance.xch_database.arn
  ]
}