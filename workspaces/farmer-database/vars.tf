
variable "master_username" {
	description = "Username for the Farmer RDS. Do NOT store in plain text. Generate with set-vars.sh."
	type        = string
	sensitive   = true
}

variable "master_password" {
	description = "Password for the Farmer RDS. Do NOT store in plain text. Generate with set-vars.sh."
	type        = string
	sensitive   = true
}

variable "bastion_subnet_id" {
	description = "Subnet ID for the bastion hosts"
	type        = string
	default     = "subnet-0ed42a73c93f2a183"
}

variable "private_subnet_id" {
	description = "Subnet ID for the private EC2s"
	type        = string
	default     = "subnet-27eff96a"
}

variable "vpc_id" {
	description = "VPC ID to deploy subnets in"
	type        = string
}

variable "env" {
	type = string
}

variable "db_identifier" {
	type = string
}

variable "db_database_name" {
	type = string
}

variable "db_port" {
	type = string
}

variable "db_instance_class" {
	type = string
}

variable "db_allocated_storage" {
	type = string
}

variable "db_max_storage" {
	type = string
}



