
variable "bastion_logs_bucket_name" {
	type = string
}

variable "bastion_pubkey" {
  type = string
}

variable "db_port" {
  type = string
}

variable "env" {
  type = string
}

variable "vpc_id" {
  type = string
}
