
data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}

data "aws_security_group" "default" {
    name = "default"
}

resource "aws_key_pair" "nat_gateway" {
  key_name   = "nat-gateway-key"
  public_key = var.instance_pub_key
}

module "nat-gateway-ec2" {
	source = "../../modules/aws/ec2/nat-gateway"

  instance_name             = "nat-gateway"
  instance_type             = "t3a.small"
  ami_id                    = "ami-000f11346bb4fc671"
  vpc_id                    = var.vpc_id

  security_group_ids        = [data.aws_security_group.default.id]

  monitoring_enabled        = false
  ec2_host_key_pair         = aws_key_pair.nat_gateway.key_name

  subnet_id_1 = data.aws_subnet.private_1b.id
  subnet_id_2 = data.aws_subnet.private_1c.id
}

output "private_ip" {
  value = module.nat-gateway-ec2.private_ip
}