terraform {
  backend "s3" {
    # Replace this with your bucket name!
    key            = "dev/nat-gateway/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}