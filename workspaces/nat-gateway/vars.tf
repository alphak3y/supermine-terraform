variable "env" {
	type = string
}

variable "instance_pub_key"{
	type = string
}

variable "vpc_id" {
	type = string
}