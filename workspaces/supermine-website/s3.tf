
resource "aws_s3_bucket" "www" {
  bucket = "www.supermine.io"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  
}

resource "aws_s3_bucket" "sandbox" {
  bucket = "sandbox.${var.env}.supermine.io"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  
}

resource "aws_s3_bucket" "polarmine" {
  bucket = "prod.polarmine.io"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}




