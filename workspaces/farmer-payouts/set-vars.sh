#! /bin/bash
echo 'Enter Chia wallet filename: '
read -s chia_key_filename
export TF_VAR_chia_key_filename=$chia_key_filename

echo 'Enter Chia wallet key: '
read -s chia_key
export TF_VAR_chia_key=$chia_key

echo 'Enter RDS DB user: '
read -s rds_db_user
export TF_VAR_rds_db_user=$rds_db_user