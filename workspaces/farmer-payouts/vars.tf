variable "chia_key_filename" {
  type   = string
  sensitive = true
}

variable "chia_key" {
  type   = string
  sensitive = true
}

variable "aws_account_number" {
  type   = string
  sensitive = false
}

variable "aws_region" {
  type   = string
  sensitive = false
  default = "us-east-1"
}

variable "rds_db_user" {
  type   = string
  sensitive = false
}

variable "env" {
  type = string
}

variable "lambda_s3_bucket" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "ami_id" {
  type = string
}