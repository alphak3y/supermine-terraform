terraform {
  backend "s3" {
    key            = "dev/farmer/payouts/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}