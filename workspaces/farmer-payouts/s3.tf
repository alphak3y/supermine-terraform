

data "aws_iam_policy_document" "artifacts" {
  statement {
    sid       = "AllowEC2"
    effect    = "Allow"
    actions   = ["s3:GetObject","s3:ListBucket"]
    resources = ["arn:aws:s3:::${var.lambda_s3_bucket}", "arn:aws:s3:::${var.lambda_s3_bucket}/db/*"]

    principals {
      type        = "AWS"
      identifiers = ["${data.aws_iam_role.payouts.arn}"]
    }
  }
}
#"arn:aws:sts::978911255009:assumed-role/PayoutsEC2"


resource "aws_s3_bucket_policy" "artifacts" {
  bucket = data.aws_s3_bucket.artifacts.id
  policy = data.aws_iam_policy_document.artifacts.json
}