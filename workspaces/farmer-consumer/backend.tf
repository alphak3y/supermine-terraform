terraform {
  backend "s3" {
    key            = "dev/xch-update-farmer/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}