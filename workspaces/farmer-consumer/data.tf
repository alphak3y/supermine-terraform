
data "aws_kinesis_stream" "farmer_api_stream_0_1_8" {
  name = "xch-farmer-api-stream-0-1-8"
}
  
data "aws_kinesis_stream" "farmer_api_stream_0_1_7" {
  name = "xch-farmer-api-stream-0-1-7"
}

data "aws_db_instance" "farmer_database"{
  db_instance_identifier = var.db_identifier
}

data "aws_iam_role" "xch_farmer_consumer"{
  name = "XCHUpdateFarmer"
}

data "aws_secretsmanager_secret" "farmer_db_password" {
  name = "rds_password"
}

data "aws_secretsmanager_secret" "farmer_db_username" {
  name = "rds_username"
}

data "aws_security_group" "default" {
  name = "default"
}

data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}
