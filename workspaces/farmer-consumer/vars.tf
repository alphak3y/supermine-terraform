
variable "farmer_consumer_function_name" {
	description = "Lambda function name for the farmer_consumer"
	type        = string
	default     = "xch-update-farmer"
}

variable "env" {
	type = string
}

variable "lambda_s3_bucket"{
	type = string
}

variable "db_identifier" {
	type = string
}