# TODO: move IAM resources to another workspace

resource "aws_iam_policy" "lambda_role_kinesis_policy" {
  name        = "kinesis_lambda_consumer"
  description = "Allow Lambda consumer to pull from Kinesis"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kinesis:DescribeStream",
        "kinesis:DescribeStreamSummary",
        "kinesis:GetRecords",
        "kinesis:GetShardIterator",
        "kinesis:ListShards",
        "kinesis:ListStreams",
        "kinesis:SubscribeToShard"
      ],
      "Effect": "Allow",
      "Resource": [
                    "${data.aws_kinesis_stream.farmer_api_stream_0_1_8.arn}"
                  ]
    },
    {
      "Action": [
        "secretsmanager:GetRandomPassword",
        "secretsmanager:GetResourcePolicy",
        "secretsmanager:GetSecretValue",
        "secretsmanager:DescribeSecret",
        "secretsmanager:ListSecretVersionIds"
      ],
      "Effect": "Allow",
      "Resource": [
        "${data.aws_secretsmanager_secret.farmer_db_username.arn}",
        "${data.aws_secretsmanager_secret.farmer_db_password.arn}"
      ]
    },
    {
      "Action": [
        "ec2:DescribeNetworkInterfaces",
        "ec2:CreateNetworkInterface",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeInstances",
        "ec2:AttachNetworkInterface"
      ],
      "Effect":   "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

module "lamba-consumer-0-1-8" {
  source = "../../modules/aws/kinesis/lambda-consumer/"

  stream_name          = "xch-farmer-api-stream-0-1-8"
  db_identifier        = var.db_identifier
  lambda_role          = "XCHUpdateFarmer"
  lambda_function_name = module.update-api-lambda-0-1-8.function_name
  batch_size           = 10000
  starting_position    = "TRIM_HORIZON"
  enabled              = true

  depends_on = [aws_iam_policy.lambda_role_kinesis_policy]
}

module "update-api-lambda-0-1-8" {
  source = "../../modules/aws/lambda/lambda-function/"

  s3_bucket      = var.lambda_s3_bucket
  s3_key         = "pool_info/pool_info_1625625088.zip"
  function_name  = "xch-update-farmer-0-1-8"
  role_name      = "${data.aws_iam_role.xch_farmer_consumer.name}"
  handler        = "lambda_function.lambda_handler"
  timeout        = "5"
  memory_size    = "128"

  security_group_ids = [data.aws_security_group.default.id]
  subnet_ids         = [data.aws_subnet.private_1b.id, data.aws_subnet.private_1c.id]

  environment_variables  = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }

}

resource "aws_iam_role_policy_attachment" "secretsmanager_attach" {
  role       = data.aws_iam_role.xch_farmer_consumer.name
  policy_arn = aws_iam_policy.xch_farmer_consumer_secretsmanager.arn
}

resource "aws_iam_policy" "xch_farmer_consumer_secretsmanager" {
  name        = "xch_farmer_consumer_secretsmanager"
  description = "Get secret values from Secrets Manager"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "secretsmanager:GetSecretValue"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}



