data "aws_apigatewayv2_api" "farmer_api_gateway" {
  api_id = "041rfrfg89"
}

# TODO: move IAM resources to another workspace
resource "aws_iam_user" "farmer_client" {
  name = "farmer_client"
  path = "/client/"

  tags = {
    tag-key = "client"
  }
}

resource "aws_iam_user_policy_attachment" "apigw_client_attach" {
  user       = aws_iam_user.farmer_client.name
  policy_arn = aws_iam_policy.farmer_client_apigw_policy.arn
}

resource "aws_iam_policy" "farmer_client_apigw_policy" {
  name        = "farmer_client"
  description = "Allow farmer client access to API Gateway"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "execute-api:Invoke"           
      ],
      "Resource": [
        "arn:aws:execute-api:us-east-1:978911255009:041rfrfg89/*",
        "arn:aws:execute-api:us-east-1:978911255009:hwgc5xbyl6/*"
      ]
    }
  ]
}
EOF
}