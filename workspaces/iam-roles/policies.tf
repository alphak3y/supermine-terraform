resource "aws_iam_policy" "farmer_apigw_kinesis_int_policy" {
  name        = "farmer_apigw_kinesis_int"
  description = "Allow farmer API GW to PutRecord to Kinesis"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kinesis:ListStreams",
        "kinesis:DescribeStream",
        "kinesis:PutRecord"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda_role_kinesis_policy" {
  name        = "kinesis_lambda_consumer"
  description = "Allow Lambda consumer to pull from Kinesis"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kinesis:DescribeStream",
        "kinesis:DescribeStreamSummary",
        "kinesis:GetRecords",
        "kinesis:GetShardIterator",
        "kinesis:ListShards",
        "kinesis:ListStreams",
        "kinesis:SubscribeToShard"
      ],
      "Effect": "Allow",
      "Resource": [
                    "${data.aws_kinesis_stream.farmer_api_stream.arn}",
                    "${data.aws_kinesis_stream.farmer_api_stream_0_1_5.arn}"
                  ]
    },
    {
      "Action": [
        "secretsmanager:GetRandomPassword",
        "secretsmanager:GetResourcePolicy",
        "secretsmanager:GetSecretValue",
        "secretsmanager:DescribeSecret",
        "secretsmanager:ListSecretVersionIds"
      ],
      "Effect": "Allow",
      "Resource": [
        "${data.aws_secretsmanager_secret.farmer_db_username.arn}",
        "${data.aws_secretsmanager_secret.farmer_db_password.arn}"
      ]
    },
    {
      "Action": [
        "ec2:DescribeNetworkInterfaces",
        "ec2:CreateNetworkInterface",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeInstances",
        "ec2:AttachNetworkInterface"
      ],
      "Effect":   "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda_role_secretsmanager" {
  name        = "lambda_role_secretsmanager"
  description = "Get secret values from Secrets Manager"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "secretsmanager:GetSecretValue"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}