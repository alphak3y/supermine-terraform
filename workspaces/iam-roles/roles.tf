

resource "aws_iam_role" "payouts" {
  name = "PayoutsEC2"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role" "xch_farmer_consumer" {
  name = "XCHUpdateFarmer"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "XCHUpdateFarmerAssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role" "web_api" {
  name = "WebAPI"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "XCHUpdateFarmerAssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role" "farmer_apigw_kinesis_int" {
  name = "farmer_apigateway_kinesis_integration"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": "AllowApiGW"
    }
  ]
}
EOF
}