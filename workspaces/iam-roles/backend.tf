terraform {
  backend "s3" {
    key            = "iam-roles/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}