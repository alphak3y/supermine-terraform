
data "aws_kinesis_stream" "farmer_api_stream"{
	name = "xch-farmer-api-stream"
}

data "aws_kinesis_stream" "farmer_api_stream_0_1_5"{
  name = "xch-farmer-api-stream-0-1-5"
}

data "aws_secretsmanager_secret" "farmer_db_password" {
  name = "rds_password"
}

data "aws_secretsmanager_secret" "farmer_db_username" {
  name = "rds_username"
}