
#### XCH UPDATE FARMER ####

resource "aws_iam_role_policy_attachment" "cloudwatch_logs_attach" {
  role       = data.aws_iam_role.xch_farmer_consumer.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "lambda_kinesis_int_attach" {
  role       = data.aws_iam_role.xch_farmer_consumer.name
  policy_arn = aws_iam_policy.xch_farmer_consumer_kinesis_policy.arn
}

#### API GW KINESIS INT ####

resource "aws_iam_role_policy_attachment" "apigw_kinesis_int_attach" {
  role       = data.aws_iam_role.farmer_apigw_kinesis_int.name
  policy_arn = aws_iam_policy.farmer_apigw_kinesis_int_policy.arn
}

## REMOVE BEFORE PRODUCTION
resource "aws_iam_role_policy_attachment" "apigw_cloudwatch_attach" {
  role       = data.aws_iam_role.farmer_apigw_kinesis_int.name
  policy_arn = data.aws_iam_policy.apigw_cloudwatch.arn
}

resource "aws_iam_role_policy_attachment" "secretsmanager_attach" {
  role       = data.aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_role_secretsmanager.arn
}