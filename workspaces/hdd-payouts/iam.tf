
data "aws_secretsmanager_secret" "farmer_db_password" {
  name = "rds_password"
}

data "aws_s3_bucket" "artifacts" {
  bucket = var.lambda_s3_bucket
}

data "aws_secretsmanager_secret" "farmer_db_username" {
  name = "rds_username"
}

data "aws_iam_role" "payouts"{
  name = "PayoutsEC2"
}

resource "aws_iam_instance_profile" "payouts" {
  name = "payouts"
  role = data.aws_iam_role.payouts.name
}

# resource "aws_iam_role" "payouts" {
#   name = "PayoutsEC2"

#   # Terraform's "jsonencode" function converts a
#   # Terraform expression result to valid JSON syntax.
#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Effect = "Allow"
#         Sid    = ""
#         Principal = {
#           Service = "ec2.amazonaws.com"
#         }
#       },
#     ]
#   })
# }


resource "aws_iam_role_policy_attachment" "payouts-cwagent-attach" {
  role       = data.aws_iam_role.payouts.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
}

resource "aws_iam_policy" "RDSCredsSecretsManager" {
  name        = "RDSCredsSecretsManager"
  description = "Allow Lambda consumer to pull from Kinesis"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "secretsmanager:GetRandomPassword",
        "secretsmanager:GetResourcePolicy",
        "secretsmanager:GetSecretValue",
        "secretsmanager:DescribeSecret",
        "secretsmanager:ListSecretVersionIds"
      ],
      "Effect": "Allow",
      "Resource": [
        "${data.aws_secretsmanager_secret.farmer_db_username.arn}",
        "${data.aws_secretsmanager_secret.farmer_db_password.arn}"
      ]
    },
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${data.aws_s3_bucket.artifacts.arn}",
        "${data.aws_s3_bucket.artifacts.arn}/db/*"
      ]
    }
  ]
}
EOF
}