variable "hddcoin_key_filename" {
  type   = string
  sensitive = true
}

variable "hddcoin_key" {
  type   = string
  sensitive = true
}

variable "aws_account_number" {
  type   = string
  sensitive = false
  default = "978911255009"
}

variable "aws_region" {
  type   = string
  sensitive = false
  default = "us-east-1"
}

variable "rds_db_user" {
  type   = string
  sensitive = false
  default = "dev"
}

variable "env" {
  type = string
  default = "dev"
}

variable "lambda_s3_bucket" {
  type = string
  default = "supermine-artifacts-dev"
}

variable "vpc_id" {
  type = string
}

variable "ami_id" {
  type = string
}