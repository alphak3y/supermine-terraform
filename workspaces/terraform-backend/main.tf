provider "aws" {
  region = "us-east-1"
  profile = var.env
}

data "aws_iam_role" "payouts" {
  name = "PayoutsEC2"
}

data "aws_iam_user" "mitic-dev" {
  user_name = "mitic-dev"
}

data "aws_iam_user" "leo-dev" {
  user_name = "leo-dev"
}

## TODO: RESTRICT DELETE ACCESS
resource "aws_s3_bucket" "terraform-backend" {
  bucket = var.terraform_bucket_name  
  # versioning will track the full history of the tfstate
  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket" "artifacts"{
  bucket = var.artifacts_bucket_name
  # versioning will track the full history of the tfstate
  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

data "aws_iam_policy_document" "artifacts" {
  statement {
    sid       = "AllowEC2"
    effect    = "Allow"
    actions   = ["s3:GetObject","s3:ListBucket"]
    resources = ["arn:aws:s3:::${var.artifacts_bucket_name}", "arn:aws:s3:::${var.artifacts_bucket_name}/db/*"]

    principals {
      type        = "AWS"
      identifiers = ["${data.aws_iam_role.payouts.arn}"]
    }
  }
}

resource "aws_s3_bucket" "assets"{
  bucket = "assets.${var.env}.supermine.io"
  # versioning will track the full history of the tfstate
  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

data "aws_iam_policy_document" "assets" {
  statement {
    sid       = "AllowFrontEndTeam"
    effect    = "Allow"
    actions   = ["s3:GetObject","s3:ListBucket","s3:PutObject","s3:DeleteObject"]
    resources = [
      "${aws_s3_bucket.assets.arn}",
      "${aws_s3_bucket.assets.arn}/*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["${data.aws_iam_user.mitic-dev.arn}","${data.aws_iam_user.leo-dev.arn}"]
    }
  }
}

resource "aws_s3_bucket_policy" "assets" {
  bucket = aws_s3_bucket.assets.id
  policy = data.aws_iam_policy_document.assets.json
}
#"arn:aws:sts::978911255009:assumed-role/PayoutsEC2"

resource "aws_s3_bucket_policy" "artifacts" {
  bucket = aws_s3_bucket.artifacts.id
  policy = data.aws_iam_policy_document.artifacts.json
}

resource "aws_s3_bucket" "website" {
  bucket = "${var.env}.supermine.io"
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  # versioning will track the full history of the tfstate
  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket" "public_artifacts" {
  bucket = "${var.env}.artifacts.supermine.io"
  acl    = "public-read"

  # versioning will track the full history of the tfstate
  versioning {
    enabled = false
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "public_artifacts" {
  bucket = aws_s3_bucket.public_artifacts.id
  policy = data.aws_iam_policy_document.public_artifacts.json
}

data "aws_iam_policy_document" "public_artifacts" {
  statement {
    sid       = "AllowPublicAccess"
    effect    = "Allow"
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${var.env}.artifacts.supermine.io/*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

data "aws_iam_policy_document" "website" {
  statement {
    sid       = "AllowPublicAccess"
    effect    = "Allow"
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${var.env}.supermine.io/*"]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}
#"arn:aws:sts::978911255009:assumed-role/PayoutsEC2"

resource "aws_s3_bucket_policy" "website" {
  bucket = aws_s3_bucket.website.id
  policy = data.aws_iam_policy_document.website.json
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"  

  attribute {
    name = "LockID"
    type = "S"
  }
}