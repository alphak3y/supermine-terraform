
module "update-pool-stats-lambda" {
  source = "../../modules/aws/lambda/lambda-function/"

  s3_bucket      = var.lambda_s3_bucket
  s3_key         = var.lambda_s3_key
  function_name  = "xch-update-pool-stats"
  role_name      = "${data.aws_iam_role.xch_farmer_consumer.name}"
  handler        = "lambda_function.lambda_handler"
  timeout        = "5"
  memory_size    = "128"

  security_group_ids = [data.aws_security_group.default.id]
  subnet_ids         = [data.aws_subnet.private_1b.id, data.aws_subnet.private_1c.id]

  environment_variables  = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

resource "aws_cloudwatch_event_rule" "update-pool-stats" {
  name        = "update-pool-stats"
  description = "Update pool stats every 5 minutes"

  schedule_expression = "cron(0/5 * * * ? *)"
}

resource "aws_cloudwatch_event_target" "update_pool_stats" {
  rule      = aws_cloudwatch_event_rule.update-pool-stats.name
  target_id = "UpdatePoolStats"
  arn       = module.update-pool-stats-lambda.arn

  depends_on = [
        aws_lambda_permission.cloudwatch_lambda
  ]
}

resource "aws_lambda_permission" "cloudwatch_lambda" {
  statement_id  = "AllowExecutionFromCloudWatchEvents"
  action        = "lambda:InvokeFunction"
  function_name = module.update-pool-stats-lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.update-pool-stats.arn}"
}

