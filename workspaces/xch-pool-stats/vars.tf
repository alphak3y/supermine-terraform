
variable "env" {
	type = string
}

variable "lambda_s3_bucket"{
	type = string
}

variable "lambda_s3_key"{
	type = string
}

variable "db_identifier" {
	type = string
}