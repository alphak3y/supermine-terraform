
data "aws_vpc" "main" {
  id = var.vpc_id
}


resource "aws_security_group" "xch_rds_access" {
  name        = "xch_rds_access"
  description = "Allow outbound to XCH Database"
  vpc_id      = data.aws_vpc.main.id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    description      = "TLS from VPC"
    from_port        = 5400
    to_port          = 5400
    protocol         = "tcp"
    security_groups  = [aws_security_group.rds_bastion_access.id]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_security_group" "bastion_to_rds" {
  name        = "bastion_to_rds"
  description = "Allow outbound to XCH Database"
  vpc_id      = data.aws_vpc.main.id
}

resource "aws_security_group" "rds_bastion_access" {
  name        = "rds_bastion_access"
  description = "Allow outbound to XCH Database"
  vpc_id      = data.aws_vpc.main.id
}

resource "aws_security_group" "farmer_payouts_sg" {
  name        = "farmer_payouts_sg"
  description = "Allow outbound to XCH Database"
  vpc_id      = data.aws_vpc.main.id
}
