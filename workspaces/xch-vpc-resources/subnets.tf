

resource "aws_subnet" "private_1b"{
  vpc_id     = data.aws_vpc.main.id
  cidr_block = var.private_1b_cidr
  availability_zone = "us-east-1b"

  tags = {
    Name = "Private 1b"
  }
}

resource "aws_subnet" "private_1c"{
  vpc_id     = data.aws_vpc.main.id
  cidr_block = var.private_1c_cidr
  availability_zone = "us-east-1c"

  tags = {
    Name = "Private 1c"
  }
}

resource "aws_subnet" "admin" {
  vpc_id     = data.aws_vpc.main.id
  cidr_block = var.admin_1c_cidr
  availability_zone = "us-east-1c"

  map_public_ip_on_launch = true

  tags = {
    Name = "Admin 1c"
  }
}
