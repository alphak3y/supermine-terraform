variable "env" {
  type   = string
}

variable "vpc_id" {
  type   = string
}

variable "private_1b_cidr" {
  type   = string
}

variable "private_1c_cidr" {
  type   = string
}

variable "admin_1c_cidr" {
  type   = string
}