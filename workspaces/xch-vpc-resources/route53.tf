
resource "aws_route53_zone" "env" {
	name = "${var.env}.supermine.io"
}

resource "aws_route53_zone" "domain" {
	name = "supermine.io"
}

