

resource "aws_ebs_volume" "payouts" {
  availability_zone = "us-east-1c"
  encrypted         = true
  size              = "40"
  type              = "gp2"
  
  tags = {
    Name = "ChiaNodeSync"
  }
}