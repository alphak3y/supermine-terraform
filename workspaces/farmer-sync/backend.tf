terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "supermine-terraform-state"
    key            = "dev/farmer/sync/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-locks"
    encrypt        = true
  }
}