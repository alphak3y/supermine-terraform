# data "aws_instance" "payouts_ec2" {
#   filter {
#     name   = "tag:Name"
#     values = ["xch-farmer-payouts"]
#   }
# }

resource "aws_secretsmanager_secret" "farmer_db_password" {
  name = "${var.farmer_db_password_secret_id}"

  recovery_window_in_days = 0
}

#"arn:aws:sts::${var.account_number}:assumed-role/PayoutsEC2/${data.aws_instance.payouts_ec2.id}"

resource "aws_secretsmanager_secret_policy" "farmer_db_password" {
  secret_arn = aws_secretsmanager_secret.farmer_db_password.arn

  policy = <<POLICY
{
  "Version" : "2012-10-17",
  "Statement" : [ {
    "Sid" : "EnableAllPermissions",
    "Effect" : "Allow",
    "Principal" : {
      "AWS" : [
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-create-farmer-token",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-login",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-register",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-logout",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-update-user-settings",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-get-user-settings",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-update-dashboard",
                "arn:aws:sts::${var.account_number}:assumed-role/XCHUpdateFarmer/xch-update-farmer",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-blacklist-user",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-change-password",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-check-blacklist",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-init-farmer"

              ]
    },
    "Action": [
                "secretsmanager:GetRandomPassword",
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
    "Resource" : "*"
  } ]
}
POLICY
}


resource "null_resource" "farmer-database-password-put-secret" {
  depends_on = [
                  aws_secretsmanager_secret.farmer_db_password
               ]

  triggers = {
    always_run = "${timestamp()}"
  }
  
  provisioner "local-exec" {
    when    = create
    command = "aws secretsmanager put-secret-value --secret-id ${var.farmer_db_password_secret_id} --secret-string ${var.farmer_db_password}"
  }
}

### FARMER USERNAME

resource "aws_secretsmanager_secret" "farmer_db_username" {
  name = "${var.farmer_db_username_secret_id}"

  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_policy" "farmer_db_username" {
  secret_arn = aws_secretsmanager_secret.farmer_db_username.arn

  policy = <<POLICY
{
  "Version" : "2012-10-17",
  "Statement" : [ {
    "Sid" : "EnableAllPermissions",
    "Effect" : "Allow",
    "Principal" : {
      "AWS" : [
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-create-farmer-token",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-login",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-register",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-logout",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-update-user-settings",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-get-user-settings",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/web-update-dashboard",
                "arn:aws:sts::${var.account_number}:assumed-role/XCHUpdateFarmer/xch-update-farmer",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-blacklist-user",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-change-password",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-check-blacklist",
                "arn:aws:sts::${var.account_number}:assumed-role/WebAPI/xch-init-farmer"
              ]
    },
    "Action": [
                "secretsmanager:GetRandomPassword",
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
    "Resource" : "*"
  } ]
}
POLICY
}

#"arn:aws:sts::${var.account_number}:assumed-role/PayoutsEC2/${data.aws_instance.payouts_ec2.id}"

resource "null_resource" "farmer-database-username-put-secret" {
  depends_on = [
                  aws_secretsmanager_secret.farmer_db_username
               ]

  triggers = {
    always_run = "${timestamp()}"
  }
  
  provisioner "local-exec" {
    when    = create
    command = "aws secretsmanager put-secret-value --secret-id ${var.farmer_db_username_secret_id} --secret-string ${var.farmer_db_username}"
  }
}