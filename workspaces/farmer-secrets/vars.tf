
variable "env"{
	type = string
}

variable "vpc_id"{
	type = string
}

variable "farmer_db_username" {
	description = "Username for the Farmer RDS. Do NOT store in plain text. Generate with set-vars.sh."
	type        = string
	sensitive   = true
}

variable "farmer_db_password" {
	description = "Password for the Farmer RDS. Do NOT store in plain text. Generate with set-vars.sh."
	type        = string
	sensitive   = true
}

variable "farmer_db_username_secret_id" {
	description = "Secret ID for the DB username"
	type        = string
	sensitive   = true
	default     = "xch_db_username"
}

variable "farmer_db_password_secret_id" {
	description = "Secret ID for the DB password"
	type        = string
	sensitive   = true
	default     = "xch_db_password"
}

variable "account_number" {
	description = "AWS account number"
	type        = string
}
