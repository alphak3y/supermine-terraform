data "aws_iam_role" "payouts" {
	name = "PayoutsEC2"
}

data "aws_iam_role" "web_api" {
	name = "WebAPI"
}

data "aws_iam_role" "farmer_consumer" {
	name = "XCHUpdateFarmer"
}

data "aws_vpc" "main" {
	id = var.vpc_id
}

data "aws_subnet" "private_1b"{
  filter {
    name   = "tag:Name"
    values = ["Private 1b"]
  }
}

data "aws_subnet" "private_1c"{
  filter {
    name   = "tag:Name"
    values = ["Private 1c"]
  }
}

data "aws_subnet" "admin_1c"{
  filter {
    name   = "tag:Name"
    values = ["Admin 1c"]
  }
}

data "aws_security_group" "default" {
	name = "default"
}