
resource "aws_vpc_endpoint" "secretsmanager" {
  vpc_id            = data.aws_vpc.main.id
  service_name      = "com.amazonaws.us-east-1.secretsmanager"
  vpc_endpoint_type = "Interface"

  subnet_ids        = [
    data.aws_subnet.private_1b.id,
    data.aws_subnet.private_1c.id
  ]

  security_group_ids = [
    data.aws_security_group.default.id
  ]

  private_dns_enabled = true
}