
resource "aws_key_pair" "payouts" {
  key_name   = "payouts-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDoLgR5nskKnHHr19hWCqcCQa/SpyNvyfA5GwV3KOwh4Qf+aaflOwKNAexUj33aJQixxzHRxBpvW+mahrNttol+yXbo2cnL9jjE7qauzW18cN0pmXf8xsLQz61CjY5JAKLzYS6XBWfnMVxt7GbS79zRC2/zBpr7KegEOljzzOsfXG1PUmvPWXpvjtRJ+a/sZQnN3VI8+sNdSK5ZMmMQS6lGYRT8RtZ4vSgFYxTsxhi3MUZgRPlPFtqAmLRWWz7AKt5D3HVU09L2283f9TC+b/5sCvEaJpcdXHeTq54h+LklIFoNkEh/TID+QeDMvYAWmZ/kYIOctSkbGfMtwEhRwJRMiGONENGmlDvwspUeDlmkfjsbpmUbLkGY9ZLWtxDrRu9xbv4IubJwyOcPquMvVDwRLteQpUN3WVe0VgYgLEvABtITBFhip9Mb0igUsuPwydReTPMKE2WRSIUTcT7JODal1Cw3crb+jiI6fVblFkn+9CrQZi0ztxdIqktGnLxtqGE= alphaqi@alphaqi-ProLiant-DL380p-Gen8"
}

resource "aws_security_group" "payouts_outbound"{
  name        = "allow_outbound"
  description = "Allow outbound traffic"
  vpc_id      = data.aws_vpc.main.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

module "payouts-ec2" {
	source = "../../modules/aws/ec2/payouts"

  instance_name             = "xch-farmer-payouts"
  instance_type             = "a1.medium"
  ami_id                    = "ami-059ff882c04ebed21"

  security_group_ids        = [
                                data.aws_security_group.default.id,
                                aws_security_group.payouts_outbound.id,
                                data.aws_security_group.rds_access.id
                              ]
                              
  iam_instance_profile_name = aws_iam_instance_profile.payouts.id


  monitoring_enabled        = false
  ec2_host_key_pair         = aws_key_pair.payouts.key_name
  
  ebs_volume_size           = "40"

  subnet_id                 = data.aws_subnet.private_1c.id

}

output "private_ip" {
  value = module.payouts-ec2.private_ip
}

