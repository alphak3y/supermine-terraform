
module "sync-farmed-blocks-lambda" {
  source = "../../modules/aws/lambda/lambda-function/"

  s3_bucket      = var.lambda_s3_bucket
  s3_key         = "sync_farmed_blocks/sync_farmed_blocks_1627411145.zip"
  function_name  = "xch-sync-farmed-blocks"
  role_name      = "${data.aws_iam_role.xch_farmer_consumer.name}"
  handler        = "lambda_function.lambda_handler"
  timeout        = "5"
  memory_size    = "128"

  security_group_ids = [data.aws_security_group.default.id]
  subnet_ids         = [data.aws_subnet.private_1b.id, data.aws_subnet.private_1c.id]

  environment_variables  = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

resource "aws_cloudwatch_event_rule" "sync-farmed-blocks" {
  name        = "sync-farmed-blocks"
  description = "Update pool stats every 5 minutes"

  schedule_expression = "cron(0/5 * * * ? *)"
}

resource "aws_cloudwatch_event_target" "sync_farmed_blocks" {
  rule      = aws_cloudwatch_event_rule.sync-farmed-blocks.name
  target_id = "SyncFarmedBlocks"
  arn       = module.sync-farmed-blocks-lambda.arn

  depends_on = [
        aws_lambda_permission.cloudwatch_lambda
  ]
}

resource "aws_lambda_permission" "cloudwatch_lambda" {
  statement_id  = "AllowExecutionFromCloudWatchEvents"
  action        = "lambda:InvokeFunction"
  function_name = module.sync-farmed-blocks-lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.sync-farmed-blocks.arn}"
}
