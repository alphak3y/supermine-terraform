
module "sync-convert-points-lambda" {
  source = "../../modules/aws/lambda/lambda-function/"

  s3_bucket      = var.lambda_s3_bucket
  s3_key         = "xch-sync-convert-points/xch-sync-convert-points_1629200014.zip"
  function_name  = "xch-sync-convert-points"
  role_name      = "${data.aws_iam_role.xch_farmer_consumer.name}"
  handler        = "lambda_function.lambda_handler"
  timeout        = "5"
  memory_size    = "128"

  security_group_ids = [data.aws_security_group.default.id]
  subnet_ids         = [data.aws_subnet.private_1b.id, data.aws_subnet.private_1c.id]

  environment_variables  = {
    db_port     = "${data.aws_db_instance.farmer_database.port}"
    db_database = "${data.aws_db_instance.farmer_database.db_name}"
    db_host     = "${data.aws_db_instance.farmer_database.address}"
  }
}

resource "aws_cloudwatch_event_rule" "sync-convert-points" {
  name        = "sync-convert-points"
  description = "Try to convert points based on earned_balance every 5 minutes"

  schedule_expression = "cron(1,6,11,16,21,26,31,36,41,46,51,56 * * * ? *)"
}

resource "aws_cloudwatch_event_target" "sync_convert_points" {
  rule      = aws_cloudwatch_event_rule.sync-convert-points.name
  target_id = "SyncConvertPoints"
  arn       = module.sync-convert-points-lambda.arn

  depends_on = [
        aws_lambda_permission.cloudwatch_lambda
  ]
}

resource "aws_lambda_permission" "cloudwatch_lambda_convert_points" {
  statement_id  = "AllowExecutionFromCloudWatchEvents"
  action        = "lambda:InvokeFunction"
  function_name = module.sync-convert-points-lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.sync-convert-points.arn}"
}

